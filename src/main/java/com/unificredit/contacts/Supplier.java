package com.unificredit.contacts;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Supplier   //extends Person 
		{
	private @Id @GeneratedValue Long id;
	
	@NotNull
	private String tax_number;
	
	@NotNull
	private String order_lead_time_in_days;
	
	public Supplier () {}
	
	public Supplier(String tax_number, String order_lead_time_in_days) {
		super();
		this.tax_number = tax_number;
		this.order_lead_time_in_days = order_lead_time_in_days;
	}

	public String getTax_number() {
		return tax_number;
	}

	public void setTax_number(String tax_number) {
		this.tax_number = tax_number;
	}

	public String getOrder_lead_time_in_days() {
		return order_lead_time_in_days;
	}

	public void setOrder_lead_time_in_days(String order_lead_time_in_days) {
		this.order_lead_time_in_days = order_lead_time_in_days;
	}
	
}

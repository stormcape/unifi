package com.unificredit.contacts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unificredit.contacts.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
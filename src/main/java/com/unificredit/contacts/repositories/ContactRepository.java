package com.unificredit.contacts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unificredit.contacts.Contact;

public interface ContactRepository  extends JpaRepository<Contact, Long> {

}

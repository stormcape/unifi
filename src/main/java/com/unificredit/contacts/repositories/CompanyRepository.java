package com.unificredit.contacts.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.unificredit.contacts.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {
	@Query(value = "SELECT * FROM contact c WHERE c.dtype = 'Company'", 
			  nativeQuery = true)
			Collection<Company> findAllCompaniesNative();
}

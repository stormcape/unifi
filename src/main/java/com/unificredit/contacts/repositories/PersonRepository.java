package com.unificredit.contacts.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.unificredit.contacts.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {
	@Query(value = "SELECT * FROM contact c WHERE c.dtype = 'Person'", 
			  nativeQuery = true)
			Collection<Person> findAllPersonsNative();
}
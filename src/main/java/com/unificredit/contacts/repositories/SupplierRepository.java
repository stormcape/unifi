package com.unificredit.contacts.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.unificredit.contacts.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {
	@Query(value = "SELECT * FROM contact c WHERE c.dtype = 'Supplier'", 
			  nativeQuery = true)
			Collection<Supplier> findAllSuppliersNative();
}
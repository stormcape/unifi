package com.unificredit.contacts;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unificredit.contacts.repositories.CompanyRepository;
import com.unificredit.contacts.repositories.ContactRepository;
import com.unificredit.contacts.repositories.CustomerRepository;
import com.unificredit.contacts.repositories.PersonRepository;
import com.unificredit.contacts.repositories.SupplierRepository;

@RestController
public class ContactController {
	Logger logger = LoggerFactory.getLogger(ContactController.class);
	private final ContactRepository contactRepository;
	private final CustomerRepository customerRepository;
	private final SupplierRepository supplierRepository;
	 private final CompanyRepository companyRepository;
	 private final PersonRepository personRepository;

	ContactController(
			 ContactRepository contactRepository
			,CustomerRepository customerRepository
			,SupplierRepository supplierRepository
			,CompanyRepository companyRepository
			,PersonRepository personRepository
					) 
	{
		this.contactRepository = contactRepository;
		this.customerRepository = customerRepository;
		this.supplierRepository = supplierRepository;
	    this.companyRepository = companyRepository;
	    this.personRepository = personRepository;
	}

	@RequestMapping("/")
	public String index() {
		return "<h1>Greetings from Unifi Credit! </h1><br>"
				+ "<a href='http://localhost:8080/contacts'>Contacts All</a><br>"
//				+ "<a href='http://localhost:8080/cust/add'>Add Customer</a><br>"
				+ "<a href='http://localhost:8080/cust'>Customer All</a><br>"
//				+ "<a href='http://localhost:8080/supplier/add'>Add Supplier</a><br>"
				+ "<a href='http://localhost:8080/suppliers'>Supplier All</a><br>" 
				+ "<br>"
				+ "<a href='http://localhost:8080/person'>Person All</a><br>"
				+ "<br>"
				+ "<a href='http://localhost:8080/company'>Company All</a><br>"
				;
	}

	@GetMapping("/contact/{contactid}")
	Optional<Contact> oneContact(@PathVariable Long contactid) {
		return 
				contactRepository.findById(contactid);
	}

	@GetMapping("/contacts")
	List<Contact> allContacts() {
		return 
				(List<Contact>) contactRepository.findAll();
	}

	@PostMapping("/contact/add")
	Contact newContact(@RequestBody Contact newContact) {
		logger.info(newContact.toString());
   	
		return contactRepository.save(newContact);
	}


	
	@GetMapping("/cust")
	List<Customer> allCustomers() {
		return customerRepository.findAll();
	}

	@GetMapping("/cust/add")
	String add() {
		logger.info("adding");
		return "<form action='http://localhost:8080/cust/add' method='POST'" + "content>"
				+ "customer_number<input name='customer_number'><br>"
				+ "last_order_date<input name='last_order_date'><br>"
				+ "GO<input type='submit' name='submit' value='Submit'><br>" + "</form>";
	}

	@PostMapping("/cust/add")
	Customer newCustomer(@RequestBody Customer newCustomer) {
		logger.info(newCustomer.toString());
		return customerRepository.save(newCustomer);
	}

	@GetMapping("/supplier/{supplierid}")
	Optional<Supplier> oneSupplier(@PathVariable Long supplierid) {
		return 
				supplierRepository.findById(supplierid);
	}

	@GetMapping("/suppliers")
	List<Supplier> allSuppliers() {
		return 
			(List<Supplier>)supplierRepository.findAllSuppliersNative();
	}

	@PostMapping("/supplier/add")
	Supplier newSupplier(@RequestBody Supplier newSupplier) {
		logger.info(newSupplier.toString());
   	
		return supplierRepository.save(newSupplier);
	}
	
	
	@GetMapping("/person") List<Person> allPersons() { 
		return 
			(List<Person>) personRepository.findAllPersonsNative(); 
	} 
	 
	@PostMapping("/person/add")
	Person newCompany(@RequestBody Person newPerson) {
		logger.info(newPerson.toString());
   	
		return personRepository.save(newPerson);
	}
		
	
	@GetMapping("/company") List<Company> allCompanies() { 
		return 
			(List<Company>) companyRepository.findAllCompaniesNative(); 
	} 
	
	@GetMapping("/company/{companyid}") 
		Optional<Company> oneCompany(@PathVariable Long companyid) { 
		return 
			 companyRepository.findById(companyid); 
	} 
	
	@PostMapping("/company/add")
	Company newCompany(@RequestBody Company newCompany) {
		logger.info(newCompany.toString());
   	
		return companyRepository.save(newCompany);
	}
	

	@GetMapping("/company/{companyid}/supplier/{supplierid}/add")
	Company  joinCompanySupplier(@PathVariable Long companyid
								,@PathVariable Long supplierid)
	{
		logger.info(companyid+ " "+supplierid);
		Company company = companyRepository.getOne(companyid);
		logger.info(company.toString());
		Supplier supplier = supplierRepository.getOne(supplierid);
		logger.info(supplier.toString());
		if (company == null || supplier == null) {
			logger.info("NOT FOUND");
			return null;
		}
		company.supplier = supplier;

		companyRepository.save(company);
		logger.info(company.toString());
		return company;
	}
	
}

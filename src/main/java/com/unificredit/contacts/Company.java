package com.unificredit.contacts;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity

public class Company extends Contact{


	private @Id @GeneratedValue Long id;
	@NotNull
	public String name;
	
	@NotNull
	public String registration_number;
	
	@OneToOne (fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	public Supplier supplier;
	
	@OneToOne (fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	public Customer customer;
	
	Company() {}

	public Company(String name, String registration_number) {
		super();
		this.name = name;
		this.registration_number = registration_number;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	@Column(nullable = false)
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getRegistration_number() {
		return registration_number;
	}

	@Column(nullable = false)
	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}

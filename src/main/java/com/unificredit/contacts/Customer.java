package com.unificredit.contacts;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class Customer // extends Company 
			{
	private @Id @GeneratedValue Long id;
	@NotNull
	private String customer_number;
	
	@NotNull
	private String last_order_date;
	
	Customer() {}
	
	public Customer(String customer_number, String last_order_date) {
		super();
		this.customer_number = customer_number;
		this.last_order_date = last_order_date;
	}
	
	public String getCustomer_number() {
		return customer_number;
	}
	public void setCustomer_number(String customer_number) {
		this.customer_number = customer_number;
	}
	public String getLast_order_date() {
		return last_order_date;
	}
	public void setLast_order_date(String last_order_date) {
		this.last_order_date = last_order_date;
	}
	
	
}

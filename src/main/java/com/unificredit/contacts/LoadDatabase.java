package com.unificredit.contacts;

import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.unificredit.contacts.repositories.CompanyRepository;
import com.unificredit.contacts.repositories.ContactRepository;
import com.unificredit.contacts.repositories.CustomerRepository;
import com.unificredit.contacts.repositories.EmployeeRepository;
import com.unificredit.contacts.repositories.PersonRepository;
import com.unificredit.contacts.repositories.SupplierRepository;


@Configuration
@Slf4j
class LoadDatabase {
	Logger logger = LoggerFactory.getLogger(LoadDatabase.class);
	
  @Bean
  CommandLineRunner initDatabase(EmployeeRepository repository,
		  CustomerRepository customerRepository,
		  PersonRepository personRepository,
		  SupplierRepository supplierRepository,
		  CompanyRepository companyRepository,
		  ContactRepository contactRepository) {
	  
	  logger.info("Preloading ");
	  long contactsCount = contactRepository.count();
	  if (contactsCount > 0 ) {
		  return args -> {logger.info("Preloading ");};
	  }
	
    return args -> {
    	
    	Customer customer = new Customer("one","15 mar");
    	customerRepository.save(customer);
    	
    	Person person = new Person("Jan","van Riebeeck");
    	person.area_code = "012";
    	person.number = "422 1111";
    	personRepository.save(person);
    	
    	Supplier supplier =  new Supplier("SARS 123","12");
    	supplierRepository.save(supplier);
    	
    	Company company = new Company("Checkers","1950/2");
    	company.supplier = supplier;
    	company.customer = customer;
    	person.area_code = "011";
    	person.number = "888 7777";
    	companyRepository.save(company);
    };

  }
}
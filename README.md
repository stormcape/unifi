# README #


### What is this repository for? ###

* Quick summary:
    
    Exercise as part of assessment.


### How do I get set up? ###


This is a standard Spring Boot project.  Clone and open in an IDE or use mvn to run.

* Configuration

Settings are in src/main/resources/application.properties
```

* Dependencies are specified in the pom.xml

* Database configuration


```
spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/unifi
spring.datasource.username=timo
spring.datasource.password=xxxxx


To use a mysql database, create an empty database as per the settings in configuration above.

NOTE : There is a bug that requires the application to be started twice on the first run.

```

* How to run tests
No tests



### Guidelines ###


The root path shows a basic menu.
To access POST endpoionts, use a tool like Postman, since the payload has to be in json, not form key value pairs.

Some Postman POST calls with payload examples:

```
Add a customer
http://localhost:8080/cust/add
{"customer_number": "No 1234",
"last_order_date": "16 Mar 2020"}

Add a company
http://localhost:8080/company/add
{"name": "Woolworths",
"registration_number": "1 Jan 1950"}


GET endpoints  (not working, example of how it needs to be done)
Add a supplier to a company

GET http://localhost:8080/company/5/supplier/6/add

```

### Who do I talk to? ###

* Repo owner or admin : timo@timo.co.za
